# ABI breaks introduced by 64-bit `time_t` on armhf

This repository contains scripts used to determine which libraries' ABIs are
impacted by switching to a 64-bit `time_t` on armhf.

The main script at the moment is `check-armhf-time_t`. It uses
`abi-compliance-checker` to build the headers of dev packages in three modes:
"base", 64-bit `time_t` and LFS (which will be implied by 64-bit `time_t` with
glibc) and dump their ABIs. Another run of `abi-compliance-checker` will diff
the ABIs in order to learn which ABIs depend upon `time_t`.

The output is stored in several files:
- abi-breaks: libraries which ABI depends upon `time_t`
- lfs-sensitive: libraries which ABI depends upon LFS
- no-change: libraries which ABI does not depend upon `time_t` or LFS
- failed-compilation: packages which headers do not build at the moment and cannot be analyzed
- uninstallable: packages which apt couldn't install (typically for debian unstable)

The status is also visible from the script's output like in the following line:

    [2023-07-11T10:46:56+00:00] liburcu-dev: time_t=[:] lfs=[false]

This says that for `liburcu-dev`, there is an ABI break for `time_t` but not
for LFS. This weird format is due to fact the code is a shell script and making
the output better-formatted risks introducing bugs (but we'll eventually get to
it).

We want all packages to be analyzed and that means making sure none ends up in
the `failed-compilation` state.

While fixing build failures, you will probably find using `--mode=dev` helpful
since it is stricter and faster (but uses more memory). While doing batch
analysis, you will probably prefer `--mode=batch` which is the default.

You can also pass --help to `check-armhf-time_t` to get more detailled usage.

